<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCalendarsTableAddVisualPreferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('calendars', function (Blueprint $table) {
			$table->json('visual_preferences')
				->after('data_presets');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('calendars', function (Blueprint $table) {
			$table->dropColumn('visual_preferences');
		});
    }
}
