<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCalendarsAddLocale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('calendars', function (Blueprint $table) {
			$table->string('locale', 16)
				->after('year');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('calendars', function (Blueprint $table) {
			$table->dropColumn('locale');
		});
    }
}
