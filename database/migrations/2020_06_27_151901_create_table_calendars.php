<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCalendars extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendars', function (Blueprint $table) {
			$table->increments('id');

			$table->string('uuid', 128);

			$table->timestamp('created_at')->useCurrent();
			$table->timestamp('updated_at')->nullable();

			$table->index('uuid', 'ix_calendars_uuid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('calendars');
	}
}
