<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCalendarsAddContent extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calendars', function (Blueprint $table) {
			$table->integer('year')
				  ->after('uuid');
			$table->json('data_presets')
				  ->after('year');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calendars', function (Blueprint $table) {
			$table->dropColumn('year');
			$table->dropColumn('data_presets');
		});
	}
}
