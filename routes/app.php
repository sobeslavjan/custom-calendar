<?php

use Illuminate\Support\Facades\Route;

/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
*/

Route::get('/', 'HomeController');

Route::prefix('/calendar')
	 ->namespace('Calendar')
	 ->group(function () {
		 Route::get('/create', 'CalendarController@create');
		 Route::get('/search', 'CalendarController@search');
		 Route::get('/{calendar}', 'CalendarController@edit');
		 Route::post('/{calendar}', 'CalendarController@update');
		 Route::get('/{calendar}/render', 'CalendarController@render');
	 });
