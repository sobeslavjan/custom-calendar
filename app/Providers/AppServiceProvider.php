<?php

namespace App\Providers;

use App\Services\Calendar\RenderService\BaseRenderService;
use App\Services\Calendar\RenderService\CachedRenderService;
use App\Services\Calendar\RenderService\RenderServiceInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// Calendar.
		$this->app->bind(RenderServiceInterface::class, function () {
			$baseRenderService = $this->app->make(BaseRenderService::class);

			return new CachedRenderService($baseRenderService);
		});
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Schema::defaultStringLength(191);
	}
}
