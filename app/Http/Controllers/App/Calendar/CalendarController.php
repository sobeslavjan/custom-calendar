<?php declare(strict_types = 1);

namespace App\Http\Controllers\App\Calendar;

use App\Helpers\UI\Notification;
use App\Http\Controllers\Controller;
use App\Model\DTOs\PageSEOMeta;
use App\Model\Entities\Calendar\Calendar;
use App\Model\Enums\Calendar\ColourSchemas;
use App\Model\Enums\Calendar\DataPresets;
use App\Model\Enums\Calendar\Locales;
use App\Model\Enums\Calendar\Sizes;
use App\Model\Enums\Calendar\Templates;
use App\Services\Calendar\RenderService\RenderServiceInterface;
use Calendar\CalendarControllerCest;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class CalendarController extends Controller
{

	/**
	 * Find existing calendar by UUID.
	 *
	 * @param Request $request
	 *
	 * @return RedirectResponse
	 *
	 * @throws ValidationException
	 */
	public function search(Request $request): RedirectResponse
	{
		$input = $this->validate($request, [
			'uuid' => ['required'],
		]);

		if (Calendar::whereUuid($input['uuid'])->exists()) {
			return redirect(sprintf('/calendar/%s', $input['uuid']));
		}

		Notification::error('Calendar not found'); // @todo translate
		return back()->withInput();
	}

	/**
	 * Edit existing calendar.
	 *
	 * @param Request  $request
	 * @param Calendar $calendar
	 *
	 * @return View
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function edit(Request $request, Calendar $calendar): View
	{
		return view(
			'app.calendar.edit',
			[
				'calendar'  => $calendar,
				'constants' => [
					'availableLocales'       => Locales::getAvailableValues(),
					'availableDataPresets'   => DataPresets::getAvailableValues(),
					'availableSizes'         => Sizes::getAvailableValues(),
					'availableTemplates'     => Templates::getAvailableValues(),
					'availableColourSchemas' => ColourSchemas::getAvailableValues(),
				],
				'meta'      => new PageSEOMeta,
			]
		);
	}

	/**
	 * @see CalendarControllerCest::calendarUpdateWorks()
	 *
	 * @param Request                $request
	 * @param Calendar               $calendar
	 * @param RenderServiceInterface $renderService
	 *
	 * @return Response
	 *
	 * @throws ValidationException
	 */
	public function update(Request $request, Calendar $calendar, RenderServiceInterface $renderService): Response
	{
		// Validate inputs.
		$input = $this->validate($request, [
			'year'                                => ['numeric', 'min:1970', 'max:2040'],
			'locale'                              => ['string', Rule::in((array) Locales::getAvailableValues())],
			'data_presets'                        => ['array'],
			'data_presets.*'                      => ['string', Rule::in((array) DataPresets::getAvailableValues())],
			'visual_preferences'                  => ['array'],
			'visual_preferences.size'             => ['string', Rule::in((array) Sizes::getAvailableValues())],
			'visual_preferences.renderMonthNumber' => ['boolean'],
			'visual_preferences.renderWeekNumber' => ['boolean'],
			'visual_preferences.renderDayNumber' => ['boolean'],
			'visual_preferences.template'         => ['string', Rule::in((array) Templates::getAvailableValues())],
			'visual_preferences.colourSchema'     => ['string', Rule::in((array) ColourSchemas::getAvailableValues())],
		]);

		// Perform DB update.
		$calendar->update([
			'year'               => $input['year'],
			'locale'             => $input['locale'],
			'data_presets'       => $input['data_presets'],
			'visual_preferences' => $input['visual_preferences'],
		]);

		// Flush renderer data.
		$renderService->flushData($calendar);

		// Return.
		if ($request->acceptsJson()) {
			return new JsonResponse();
		}

		return back();
	}

	/**
	 * Render the calendar.
	 *
	 * @param Request                $request
	 * @param Calendar               $calendar
	 * @param RenderServiceInterface $renderService
	 *
	 * @return View
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function render(Request $request, Calendar $calendar, RenderServiceInterface $renderService): View
	{
		return $renderService->render($calendar);
	}

	/**
	 * Create new calendar instance.
	 *
	 * @return RedirectResponse
	 */
	public function create(): RedirectResponse
	{
		$calendar = Calendar::create([
			'uuid'               => Uuid::uuid4(),
			'year'               => Carbon::now()->year,
			'locale'             => Locales::EN,
			'data_presets'       => [],
			'visual_preferences' => [
				'size'             => Sizes::A4,
				'renderMonthNumber' => false,
				'renderWeekNumber' => false,
				'renderDayNumber' => false,
				'template'         => Templates::DEFAULT,
				'colourSchema'     => ColourSchemas::DEFAULT,
			],
		]);

		Notification::success('Calendar created. Save the ID, in case you wanted to return to it.'); // @todo translate
		return redirect(sprintf('/calendar/%s', $calendar->uuid));
	}
}
