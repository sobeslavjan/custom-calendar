<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Model\DTOs\PageSEOMeta;
use Illuminate\View\View;

class HomeController extends Controller
{

	/**
	 * @return View
	 */
	public function __invoke(): View
	{
		return view('app.home', ['meta' => new PageSEOMeta]);
	}
}
