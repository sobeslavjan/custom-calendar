<?php declare(strict_types=1);

namespace App\Model\DTOs\Calendar;

use App\Model\Enums\Calendar\HighlightLevels;
use Carbon\Carbon;
use Spatie\LaravelData\Data;

class Entry extends Data
{

	/** @var Carbon $date */
	public Carbon $date;

	/** @var string|null $caption Short caption describing meaning of the day. */
	public ?string $caption;

	/** @var string|null $note Note, explaining the caption in greater detail. */
	public ?string $note;

	/** @var string|null $icon Small icon (emoji). */
	public ?string $icon;

	/** @var string|null $image Raster image URI. */
	public ?string $image;

	/** @var integer Highlight level. @see \App\Model\Enums\Calendar\HighlightLevels::class */
	public int $highlight = HighlightLevels::REGULAR;

	/** @var string|null $class Custom class for rendering purposes. */
	public ?string $class;

	/** @var string|null $hash Hash of content for duplicate prevention. */
	public ?string $hash;

	/**
	 * @param array $args
	 *
	 * @return static
	 */
	public static function build(array $args): static
	{
		$instance = parent::from($args);

		// Generate hash, if necessary.
		if (empty($instance->hash)) {
			$instance->hash = sha1(
				$instance->date->format('md')
				. $instance->caption
				. $instance->icon
			);
		}

		return $instance;
	}
}
