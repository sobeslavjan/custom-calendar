<?php declare(strict_types = 1);

namespace App\Model\DTOs\Calendar;

use Carbon\Carbon;
use Spatie\LaravelData\Data;

class Week extends Data
{

	use HasEntryIndex;

	/** @var integer|null $weekOfYear */
	public ?int $weekOfYear;

	/** @var Day[] $days */
	public array $days = [];

	/**
	 * @param Carbon $date
	 *
	 * @return Day|null Reference to Day object.
	 */
	public function &getDayReference(Carbon $date): ?Day
	{
		return $this->days[$date->dayOfWeekIso];
	}
}
