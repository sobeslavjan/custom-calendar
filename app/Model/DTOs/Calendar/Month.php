<?php declare(strict_types = 1);

namespace App\Model\DTOs\Calendar;

use Carbon\Carbon;
use Spatie\LaravelData\Data;

class Month extends Data
{

	use HasEntryIndex;

	/** @var Week[] $weeks */
	public array $weeks = [];

	/**
	 * @param Carbon $date
	 *
	 * @return Week|null Reference to Week object.
	 */
	public function &getWeekReference(Carbon $date): ?Week
	{
		return $this->weeks[$date->weekNumberInMonth];
	}
}
