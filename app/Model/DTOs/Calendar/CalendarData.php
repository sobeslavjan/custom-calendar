<?php declare(strict_types = 1);

namespace App\Model\DTOs\Calendar;

use App\Model\Enums\Calendar\ColourSchemas;
use App\Model\Enums\Calendar\Sizes;
use App\Model\Enums\Calendar\Templates;
use App\Services\Calendar\Asset;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Spatie\LaravelData\Data;

class CalendarData extends Data
{

	// Base settings.

	/** @var integer $year Year of the calendar. */
	public int $year;

	/** @var string $locale Internationalization preferences, e.g. `cs`. */
	public string $locale;

	// Visual preferences.

	/** @var string $size Output dimensions, e.g. `A4`. */
	public string $size = Sizes::A4;

	/** @var boolean $renderMonthNumber */
	public bool $renderMonthNumber = false;

	/** @var boolean $renderWeekNumber */
	public bool $renderWeekNumber = false;

	/** @var boolean $renderDayNumber */
	public bool $renderDayNumber = false;

	/** @var string $template */
	public string $template = Templates::DEFAULT;

	/** @var string $colourSchema */
	public string $colourSchema = ColourSchemas::DEFAULT;

	// Internal.

	/** @var Month[] $months Root of full logical representation of the calendar. */
	public array $months = [];

	/** @var Entry[] $entries Contains all entries data. */
	public array $entries = [];

	/** @var Asset[] $assets Contains files related to the calendar. */
	public array $assets = [];

	/**
	 * @param integer $year
	 * @param string  $locale
	 * @param array   $visualPreferences
	 *
	 * @return static
	 */
	public static function build(int $year, string $locale, array $visualPreferences): static
	{
		$instance = static::from($visualPreferences);

		$instance->year   = $year;
		$instance->locale = $locale;

		// Prepare full logical representation of the calendar.
		$start  = Carbon::createStrict($year)->startOfYear();
		$end    = Carbon::createStrict($year)->endOfYear();
		$period = CarbonPeriod::create($start, $end);

		foreach ($period as $date) {
			/** @var Carbon $date */

			// Prepare Month object, if necessary.
			if (empty($instance->getMonthReference($date))) {
				$instance->months[$date->month] = new Month();
			}
			$month =& $instance->getMonthReference($date);

			// Prepare Week object, if necessary.
			if (empty($month->getWeekReference($date))) {
				$month->weeks[$date->weekNumberInMonth] = Week::from([
					'weekOfYear' => $date->weekOfYear,
					'days'       => [
						1 => new Day(),
						2 => new Day(),
						3 => new Day(),
						4 => new Day(),
						5 => new Day(),
						6 => new Day(),
						7 => new Day(),
					],
				]);
			}
			$week =& $month->getWeekReference($date);

			// Set Day's day of month property.
			$day             =& $week->getDayReference($date);
			$day->dayOfMonth = $date->day;
		}

		return $instance;
	}

	/**
	 * @param Carbon $date
	 *
	 * @return Month|null Reference to Month object.
	 */
	public function &getMonthReference(Carbon $date): ?Month
	{
		return $this->months[$date->month];
	}

	/**
	 * @param Asset $asset
	 *
	 * @return void
	 */
	public function addAsset(Asset $asset): void
	{
		$this->assets[$asset->getAssetType()] = $asset;
	}

	/**
	 * @param Entry $entry
	 *
	 * @return void
	 */
	public function addEntry(Entry $entry): void
	{
		// Get equivalent of the Entry's date in the year of the calendar.
		$dateEquivalent = clone $entry->date;
		$dateEquivalent->year($this->year);

		// Store entry.
		$this->entries[$entry->hash] = $entry;
		$entryReference              =& $this->entries[$entry->hash];

		// Index entry.
		$month = $this->getMonthReference($dateEquivalent);
		$month->indexEntry($entryReference);

		$week = $month->getWeekReference($dateEquivalent);
		$week->indexEntry($entryReference);

		$day = $week->getDayReference($dateEquivalent);
		$day->indexEntry($entryReference);
	}
}
