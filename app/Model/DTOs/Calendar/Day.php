<?php declare(strict_types = 1);

namespace App\Model\DTOs\Calendar;

use Spatie\LaravelData\Data;

class Day extends Data
{

	use HasEntryIndex;

	/** @var integer|null $dayOfMonth */
	public ?int $dayOfMonth;
}
