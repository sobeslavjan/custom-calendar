<?php

namespace App\Model\DTOs\Calendar;

use App\Model\Enums\Calendar\HighlightLevels;

trait HasEntryIndex
{

	/** @var Entry[] $entriesIndex */
	public array $entriesIndex = [];

	/**
	 * @param Entry $entry
	 *
	 * @return void
	 */
	public function indexEntry(Entry $entry): void
	{
		$this->entriesIndex[$entry->hash] = $entry;
	}

	/**
	 * @return integer
	 */
	public function getHighestHighlight(): int
	{
		return collect($this->entriesIndex)
				   ->max('highlight')
			   ?? HighlightLevels::REGULAR;
	}

	/**
	 * @return Entry[]
	 */
	public function getEntriesWithIcons(): array
	{
		return collect($this->entriesIndex)
			->whereNotNull('icon')
			->all();
	}

	/**
	 * @return Entry[]
	 */
	public function getEntriesWithCaptions(): array
	{
		return collect($this->entriesIndex)
			->whereNotNull('caption')
			->all();
	}

	/**
	 * @return Entry[]
	 */
	public function getEntriesWithImages(): array
	{
		return collect($this->entriesIndex)
			->whereNotNull('image')
			->all();
	}

	/**
	 * @return Entry[]
	 */
	public function getEntriesWithNotes(): array
	{
		return collect($this->entriesIndex)
			->whereNotNull('note')
			->sortBy(function (Entry $entry) {
				return $entry->date->format('d');
			})
			->all();
	}

	/**
	 * @return string[]
	 */
	public function getClasses(): array
	{
		return collect($this->entriesIndex)
			->pluck('class')
			->all();
	}
}
