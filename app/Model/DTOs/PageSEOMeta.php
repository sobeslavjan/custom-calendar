<?php declare(strict_types = 1);

namespace App\Model\DTOs;

use Spatie\LaravelData\Data;

class PageSEOMeta extends Data
{

	/** @var string $robots */
	public $robots;

	/** @var string|null */
	public $canonicalUri = null;

	/** @var string $lang */
	public $lang;

	/** @var string $locale */
	public $locale;

	/** @var string $charset */
	public $charset;

	/** @var string $rootTitle */
	public $rootTitle;

	/** @var string $title */
	public $title;

	/** @var string $description */
	public $description;

	/** @var string|null $keywords */
	public $keywords;

	/** @var string $image */
	public $image;

	/** @var string $imageMime */
	public $imageMime;

	/** @var string */
	public $author;

	/** @var string */
	public $authorLink;

	/**
	 * @param string[] $overrideSettings
	 */
	public function __construct(array $overrideSettings = [])
	{
		// Indexing robots.
		$this->robots       = $overrideSettings['robots'] ?? 'index,follow';
		$this->canonicalUri = $overrideSettings['canonicalUri'] ?? null;

		// Language.
		$this->lang    = $overrideSettings['lang'] ?? translate('site.lang');
		$this->locale  = $overrideSettings['locale'] ?? translate('site.locale');
		$this->charset = $overrideSettings['charset'] ?? translate('site.charset');

		// Page contents.
		$this->rootTitle   = translate('site.rootTitle');
		$this->title       = sprintf(
			'%s | %s',
			$this->rootTitle,
			$overrideSettings['title'] ?? ''
		);
		$this->description = $overrideSettings['description'] ?? translate('site.description');
		$this->keywords    = sprintf(
			'%s %s',
			translate('site.keywords'),
			$overrideSettings['keywords'] ?? ''
		);
		$this->image       = translate('site.image');
		$this->imageMime   = translate('site.imageMime');

		// Author.
		$this->author     = $overrideSettings['author'] ?? translate('site.author');
		$this->authorLink = $overrideSettings['authorLink'] ?? translate('site.authorLink');
	}
}
