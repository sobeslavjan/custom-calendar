<?php declare(strict_types = 1);

namespace App\Model\Enums\Calendar;

use Consistence\Enum\Enum;

class HighlightLevels extends Enum
{

	public const REGULAR     = 1;
	public const INTERESTING = 2;
	public const HOLIDAY     = 3;
	public const IMPORTANT   = 4;
	public const CRUCIAL     = 5;
}
