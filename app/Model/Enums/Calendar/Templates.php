<?php declare(strict_types = 1);

namespace App\Model\Enums\Calendar;

use Consistence\Enum\Enum;

class Templates extends Enum
{

	public const DEFAULT    = 'default';
	public const MINIMALIST = 'minimalist';
}
