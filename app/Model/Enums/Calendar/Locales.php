<?php declare(strict_types = 1);

namespace App\Model\Enums\Calendar;

use Consistence\Enum\Enum;

class Locales extends Enum
{

	public const CS = 'cs';
	public const EN = 'en';
}
