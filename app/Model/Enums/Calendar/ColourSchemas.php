<?php declare(strict_types = 1);

namespace App\Model\Enums\Calendar;

use Consistence\Enum\Enum;

class ColourSchemas extends Enum
{

	public const DEFAULT       = 'default';
	public const RAINBOW       = 'rainbow';
	public const MONOCHROMATIC = 'monochromatic';
}
