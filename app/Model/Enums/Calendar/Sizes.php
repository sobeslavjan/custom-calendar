<?php declare(strict_types = 1);

namespace App\Model\Enums\Calendar;

use Consistence\Enum\Enum;

class Sizes extends Enum
{

	public const A5           = 'A5';
	public const A5_LANDSCAPE = 'A5 landscape';

	public const A4           = 'A4';
	public const A4_LANDSCAPE = 'A4 landscape';

	public const A3           = 'A3';
	public const A3_LANDSCAPE = 'A3 landscape';
}
