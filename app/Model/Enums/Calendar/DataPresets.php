<?php declare(strict_types = 1);

namespace App\Model\Enums\Calendar;

use Consistence\Enum\Enum;

class DataPresets extends Enum
{

	public const CZ_STATE_HOLIDAYS       = 'cz/state-holidays';
	public const CZ_OTHER_HOLIDAYS       = 'cz/other-holidays';
	public const CZ_IMPORTANT_DAYS       = 'cz/important-days';
	public const CZ_NAMEDAYS             = 'cz/namedays';
	public const CZ_CLOSED_STORES        = 'cz/closed-stores';
	public const CZ_DAYLIGHT_SAVING_TIME = 'cz/daylight-saving-time';

	public const SPECIAL_INTERNATIONAL_DAYS  = 'special/international-days';
	public const SPECIAL_ASTRONOMICAL_EVENTS = 'special/astronomical-events';
	public const SPECIAL_PAGAN_HOLIDAYS      = 'special/pagan-holidays';
	public const SPECIAL_FANDOM_DAYS         = 'special/fandom-days';
}
