<?php

namespace App\Model\Entities\Calendar;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{

	/** @var string $table */
	public $table = 'calendars';

	/** @var array<string,string> */
	public $casts = [
		'year' => 'integer',
		'data_presets' => 'array',
		'visual_preferences' => 'array',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];

	/** @var string[] $fillable */
	public $fillable = [
		'uuid',
		'year',
		'locale',
		'data_presets',
		'visual_preferences',
	];

	/** @var boolean $timestamps */
	public $timestamps = true;

	/**
	 * @return string
	 */
	public function getRouteKeyName()
	{
		return 'uuid';
	}
}
