<?php declare(strict_types=1);

namespace App\Services\Calendar\Calculators;

abstract class Calculator implements CalculatorInterface
{

	/**
	 * @param integer $year Year that will be used for calculations of variable dates (e.g. moving holidays).
	 */
	public function __construct(protected int $year)
	{
	}
}
