<?php declare(strict_types = 1);

namespace App\Services\Calendar\Calculators;

use Carbon\Carbon;

interface CalculatorInterface
{

	/**
	 * Returns all occurrences in set year.
	 *
	 * @return Carbon[]
	 */
	public function getAllOccurrences(): array;
}
