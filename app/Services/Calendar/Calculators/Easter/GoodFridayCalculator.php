<?php declare(strict_types = 1);

namespace App\Services\Calendar\Calculators\Easter;

use App\Services\Calendar\Calculators\CalculatorInterface;
use Carbon\Carbon;

class GoodFridayCalculator extends EasterSundayCalculator implements CalculatorInterface
{

	/**
	 * @inheritDoc
	 *
	 * @return Carbon
	 */
	protected function getFirstOccurrence(): Carbon
	{
		return parent::getFirstOccurrence()->subDays(2);
	}
}
