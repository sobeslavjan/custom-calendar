<?php declare(strict_types = 1);

namespace App\Services\Calendar\Calculators\Easter;

use App\Services\Calendar\Calculators\Calculator;
use App\Services\Calendar\Calculators\CalculatorInterface;
use Carbon\Carbon;

class EasterSundayCalculator extends Calculator implements CalculatorInterface
{


	/**
	 * @inheritDoc
	 *
	 * @return Carbon
	 */
	protected function getFirstOccurrence(): Carbon
	{
		return Carbon::createStrict($this->year, 3, 21)
			->addDays(\easter_days($this->year));
	}

	/**
	 * @inheritDoc
	 *
	 * @return Carbon[]
	 */
	public function getAllOccurrences(): array
	{
		return [
			$this->getFirstOccurrence(),
		];
	}
}
