<?php declare(strict_types = 1);

namespace App\Services\Calendar\Calculators\Solar;

use App\Services\Calendar\Calculators\CalculatorInterface;
use Carbon\Carbon;
use Exception;

class SummerSolsticeCalculator extends AbstractSolarCalculator implements CalculatorInterface
{

	/**
	 * @inheritDoc
	 *
	 * @return Carbon[]
	 *
	 * @throws Exception
	 */
	public function getAllOccurrences(): array
	{
		return [
			$this->calculateCarbon($this->year, static::JUN),
		];
	}
}
