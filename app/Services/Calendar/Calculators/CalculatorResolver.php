<?php declare(strict_types = 1);

namespace App\Services\Calendar\Calculators;

use InvalidArgumentException;

class CalculatorResolver
{

	/**
	 * Returns calculator based on the identifier.
	 *
	 * @see CalculatorIdentifiers for available values.
	 *
	 * @param string  $identifier
	 * @param integer $calculatorYear
	 *
	 * @return CalculatorInterface
	 *
	 */
	public static function resolveCalculator(string $identifier, int $calculatorYear): CalculatorInterface
	{
		return match ($identifier) {
			// Solar.
			CalculatorIdentifiers::SPRING_EQUINOX => new Solar\SpringEquinoxCalculator($calculatorYear),
			CalculatorIdentifiers::SUMMER_SOLSTICE => new Solar\SummerSolsticeCalculator($calculatorYear),
			CalculatorIdentifiers::AUTUMN_EQUINOX => new Solar\AutumnEquinoxCalculator($calculatorYear),
			CalculatorIdentifiers::WINTER_SOLSTICE => new Solar\WinterSolsticeCalculator($calculatorYear),

			// Lunar.
			CalculatorIdentifiers::NEW_MOON => new Lunar\NewMoonCalculator($calculatorYear),
			CalculatorIdentifiers::FIRST_QUARTER_MOON => new Lunar\FirstQuarterMoonCalculator($calculatorYear),
			CalculatorIdentifiers::FULL_MOON => new Lunar\FullMoonCalculator($calculatorYear),
			CalculatorIdentifiers::LAST_QUARTER_MOON => new Lunar\LastQuarterMoonCalculator($calculatorYear),

			// Easter.
			CalculatorIdentifiers::EASTER_GOOD_FRIDAY => new Easter\GoodFridayCalculator($calculatorYear),
			CalculatorIdentifiers::EASTER_SUNDAY => new Easter\EasterSundayCalculator($calculatorYear),
			CalculatorIdentifiers::EASTER_MONDAY => new Easter\EasterMondayCalculator($calculatorYear),

			default => throw new InvalidArgumentException('Invalid calculator identifier supplied')
		};
	}
}
