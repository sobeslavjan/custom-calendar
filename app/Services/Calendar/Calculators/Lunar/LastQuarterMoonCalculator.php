<?php declare(strict_types = 1);

namespace App\Services\Calendar\Calculators\Lunar;

use App\Services\Calendar\Calculators\CalculatorInterface;
use Carbon\Carbon;
use Exception;

class LastQuarterMoonCalculator extends AbstractLunarCalculator implements CalculatorInterface
{

	/**
	 * @inheritDoc
	 *
	 * @return Carbon[]
	 *
	 * @throws Exception
	 */
	public function getAllOccurrences(): array
	{
		return $this->calculateAll($this->year, static::LAST_QUARTER_MOON);
	}
}
