<?php declare(strict_types = 1);

namespace App\Services\Calendar\Calculators\Lunar;

use App\Services\Calendar\Calculators\Calculator;
use Carbon\Carbon;
use Solaris\MoonPhase;

/**
 * Calculator for lunar phases (e.g. new moon, full moon).
 */
abstract class AbstractLunarCalculator extends Calculator
{

	// Phase constants.
	protected const NEW_MOON           = 4;
	protected const FIRST_QUARTER_MOON = 5;
	protected const FULL_MOON          = 6;
	protected const LAST_QUARTER_MOON  = 7;

	/**
	 * @param integer $year
	 * @param integer $phase Event identifier. See constants `static::NEW_MOON`, `static::FIRST_QUARTER_MOON`, `static::FULL_MOON`, and `static:LAST_QUARTER_MOON`.
	 *
	 * @return Carbon[] Array of all occurrences.
	 */
	protected function calculateAll(int $year, int $phase): array
	{
		// Set valid timestamp bounds.
		$minDateTime = Carbon::createStrict($year)->startOfYear();
		$maxDateTime = Carbon::createStrict($year)->endOfYear();

		// Start searching a month before actual start of the year, so that the first phase is surely included (underlying class searches for the next phase).
		$iteratorDateTime = Carbon::createStrict($year)->startOfYear()->subMonth();
		$dates             = [];

		while ($iteratorDateTime->isBefore($maxDateTime)) {
			// The MoonPhase::getPhase* methods return the same occurrence, if the passed timestamp matches exactly.
			// Start searching a day after the previous date, in order to avoid endless loop.
			$iteratorDateTime->addDay();

			$moon              = new MoonPhase($iteratorDateTime);
			$nextOccurrenceTimestamp = match ($phase) {
				static::NEW_MOON => $moon->getPhaseNextNewMoon(),
				static::FIRST_QUARTER_MOON => $moon->getPhaseNextFirstQuarter(),
				static::FULL_MOON => $moon->getPhaseNextFullMoon(),
				static::LAST_QUARTER_MOON => $moon->getPhaseNextLastQuarter(),
				default => throw new \InvalidArgumentException('Invalid moon phase supplied')
			};
			$iteratorDateTime = Carbon::createFromTimestamp($nextOccurrenceTimestamp);

			// Double-check that the date is within validity bounds.
			if ($iteratorDateTime->isAfter($minDateTime)
				&& $iteratorDateTime->isBefore($maxDateTime)
			) {
				$dates[] = $iteratorDateTime;
			}

			// Prevent endless loop.
			if (count($dates) > 50) {
				throw new \InvalidArgumentException('highly suspicious amount of moon phases per year');
			}
		}

		return $dates;
	}
}
