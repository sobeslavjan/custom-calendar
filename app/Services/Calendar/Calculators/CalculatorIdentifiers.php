<?php declare(strict_types = 1);

namespace App\Services\Calendar\Calculators;

use Consistence\Enum\Enum;

class CalculatorIdentifiers extends Enum
{

	// Solar.
	public const SPRING_EQUINOX  = 'calculate:spring-equinox';
	public const SUMMER_SOLSTICE = 'calculate:summer-solstice';
	public const AUTUMN_EQUINOX  = 'calculate:autumn-equinox';
	public const WINTER_SOLSTICE = 'calculate:winter-solstice';

	// Lunar.
	public const NEW_MOON           = 'calculate:new-moon';
	public const FIRST_QUARTER_MOON = 'calculate:first-quarter-moon';
	public const FULL_MOON          = 'calculate:full-moon';
	public const LAST_QUARTER_MOON  = 'calculate:last-quarter-moon';

	// Easter.
	public const EASTER_GOOD_FRIDAY = 'calculate:easter-good-friday';
	public const EASTER_SUNDAY      = 'calculate:easter-sunday';
	public const EASTER_MONDAY      = 'calculate:easter-monday';
}
