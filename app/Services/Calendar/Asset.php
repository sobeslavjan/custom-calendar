<?php

namespace App\Services\Calendar;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

class Asset
{

	public const ASSET_TYPE_ENTRIES = 'entries';
	public const ASSET_TYPE_FRONT_PAGE = 'frontPage';
	public const ASSET_TYPE_LAST_PAGE = 'lastPage';
	public const ASSET_TYPE_STYLE = 'style';

	protected const FILEPATHS = [
		self::ASSET_TYPE_ENTRIES    => '/user-data/%s/entries.json',
		self::ASSET_TYPE_FRONT_PAGE => '/user-data/%s/front-page.html',
		self::ASSET_TYPE_LAST_PAGE => '/user-data/%s/last-page.html',
		self::ASSET_TYPE_STYLE      => '/user-data/%s/style.css',
	];

	/** @var string $assetType E.g. `entries`. */
	protected string $assetType;

	/** @var string $storage E.g. `local`. */
	protected string $storage;

	/** @var string $filename E.g. `user-data/959545d2-ba58-4411-8d59-ef62dbe55230/entries.json`. */
	protected string $filename;

	/**
	 * Static constructor.
	 *
	 * @param string $assetType    E.g. `entries`.
	 * @param string $calendarUuid E.g. `959545d2-ba58-4411-8d59-ef62dbe55230`.
	 *
	 * @return static|null Returns null, if asset couldn't be found.
	 */
	public static function local(string $assetType, string $calendarUuid): ?static
	{
		$instance            = new static();
		$instance->assetType = $assetType;
		$instance->storage   = 'local';
		$instance->filename  = sprintf(static::FILEPATHS[$assetType], $calendarUuid);

		// Verify existence.
		if (! Storage::disk($instance->storage)->exists($instance->getFilename())) {
			return null;
		}

		return $instance;
	}

	/**
	 * @return string
	 */
	public function getAssetType(): string
	{
		return $this->assetType;
	}

	/**
	 * @return string
	 */
	public function getFilename(): string
	{
		return $this->filename;
	}

	/**
	 * @return string Contents of the file, e.g. `<html></html>`.
	 *
	 * @throws FileNotFoundException
	 */
	public function getContents(): string
	{
		return Storage::disk($this->storage)
			->get($this->getFilename());
	}
}
