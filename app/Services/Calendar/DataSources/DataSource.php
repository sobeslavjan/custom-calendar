<?php declare(strict_types=1);

namespace App\Services\Calendar\DataSources;

use App\Model\DTOs\Calendar\Entry;
use App\Services\Calendar\Calculators\CalculatorIdentifiers;
use App\Services\Calendar\DataSources\Traits\UsesCalculators;
use App\Services\Calendar\DataSources\Traits\UsesWildcardProcessors;
use App\Services\Calendar\WildcardProcessors\WildcardProcessorResolver;
use Carbon\Carbon;

abstract class DataSource
{

	use UsesCalculators;
	use UsesWildcardProcessors;

	/**
	 * @param string $input Either date, e.g. `2020-12-31`, Carbon compliant string, e.g. `last sunday of march 2021`, or calculator identifier, e.g `calculate:easter-sunday`.
	 *
	 * @return Carbon[] Array of possible dates. Single date string may translate to multiple actual dates.
	 */
	protected function parseDate(string $input): array
	{
		$input = $this->parseString($input);

		return $this->usesCalculators() && in_array($input, (array) CalculatorIdentifiers::getAvailableValues())
			? $this->resolveCalculator($input)->getAllOccurrences()
			: [Carbon::parse($input)];
	}

	/**
	 * @param string     $input Unprocessed string.
	 * @param Entry|null $entry If supplied, Entry aware processors will be used as well.
	 *
	 * @return string Processed string.
	 */
	protected function parseString(string $input, ?Entry $entry = null): string
	{
		if ($this->usesWildcardProcessors()) {
			foreach ($this->getWildcardProcessors($entry) as $processor) {
				$input = $processor->parse($input);
			}
		}

		return $input;
	}
}
