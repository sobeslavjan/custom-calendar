<?php declare(strict_types = 1);

namespace App\Services\Calendar\DataSources\Plain;

use App\Model\DTOs\Calendar\Entry;
use App\Services\Calendar\DataSources\DataSource;
use App\Services\Calendar\DataSources\DataSourceInterface;
use Carbon\Exceptions\InvalidFormatException;
use Generator;

abstract class JsonDataSource extends DataSource implements DataSourceInterface
{

	/** @var string $filename */
	protected string $filename;

	/**
	 * Iterate over input and yield one date at the time.
	 *
	 * @return Generator
	 *
	 * @throw \InvalidArgumentException If the file contents couldn't be decoded as JSON.
	 */
	public function iterate(): Generator
	{
		$entries = json_decode(
			(string) file_get_contents($this->filename),
			true
		);

		// Handle invalid JSON.
		if ($entries === null) {
			throw new \InvalidArgumentException('The file does not contain a valid JSON');
		}

		foreach ($entries as $entry) {
            // Skip comments.
            if (!is_array($entry)) {
                continue;
            }

			try {
				// Single date string may translate to multiple actual dates. Yield entry for each of those possible dates.
				$possibleDates = $this->parseDate($entry['date']);

                foreach ($possibleDates as $date) {
                    $entry['date'] = $date;

                    $entryDto = Entry::build($entry);

                    if ($entryDto->caption) {
                        $entryDto->caption = $this->parseString($entryDto->caption, $entryDto);
                    }
                    if ($entryDto->note) {
                        $entryDto->note = $this->parseString($entryDto->note, $entryDto);
                    }
                    if ($entryDto->icon) {
                        $entryDto->icon = $this->parseString($entryDto->icon, $entryDto);
                    }

                    yield $entryDto;
				}
			} catch (InvalidFormatException $exception) {
				// Date couldn't be parsed. No problem.
			}
		}
	}
}
