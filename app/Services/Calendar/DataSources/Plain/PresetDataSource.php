<?php declare(strict_types = 1);

namespace App\Services\Calendar\DataSources\Plain;

use App\Model\Enums\Calendar\DataPresets;
use App\Services\Calendar\DataSources\DataSourceInterface;

class PresetDataSource extends JsonDataSource implements DataSourceInterface
{

	/**
	 * @see DataPresets for available preset names.
	 *
	 * @param string $presetName E.g. `cz/namedays`.
	 */
	public function __construct(string $presetName)
	{
		// Validate, if preset is valid and usable.
		if (! DataPresets::isValidValue($presetName)) {
			throw new \InvalidArgumentException('Invalid preset name');
		}

		$filename = resource_path(sprintf('data/%s.json', $presetName));
		if (! is_file($filename)) {
			throw new \InvalidArgumentException('File for valid data preset not found.');
		}

		$this->filename = $filename;
	}
}
