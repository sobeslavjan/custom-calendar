<?php declare(strict_types = 1);

namespace App\Services\Calendar\DataSources\Plain;

use App\Services\Calendar\DataSources\DataSourceInterface;
use Illuminate\Support\Facades\Storage;

class LocalStorageDataSource extends JsonDataSource implements DataSourceInterface
{

	/**
	 * @param string $filename Local file path relative to `/storage/` folder. E.g. `/app/data/my-family-birthdays.json`.
	 */
	public function __construct(string $filename)
	{
		$storage = Storage::disk('local');

		if (! $storage->exists($filename)) {
			throw new \InvalidArgumentException('File for valid data preset not found.');
		}

		$this->filename = $storage->path($filename);
	}
}
