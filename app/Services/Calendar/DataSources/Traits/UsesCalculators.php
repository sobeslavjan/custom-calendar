<?php declare(strict_types = 1);

namespace App\Services\Calendar\DataSources\Traits;

use App\Services\Calendar\Calculators\CalculatorInterface;
use App\Services\Calendar\Calculators\CalculatorResolver;
use InvalidArgumentException;

trait UsesCalculators
{

	/** @var integer|null $calculatorYear */
	protected ?int $calculatorYear;

	/**
	 * @param integer|null $calculatorYear Year that will be used for calculations of variable dates (e.g. moving holidays).
	 *
	 * @return void
	 */
	public function useCalculators(?int $calculatorYear = null): void
	{
		$this->calculatorYear = $calculatorYear;
	}

	/**
	 * @return boolean
	 */
	public function usesCalculators(): bool
	{
		return ! empty($this->calculatorYear);
	}

	/**
	 * Returns calculator based on the identifier.
	 *
	 * @see CalculatorIdentifiers for available values.
	 *
	 * @param string $identifier
	 *
	 * @return CalculatorInterface
	 *
	 * @throws InvalidArgumentException If the class does not use calculators, or invalid identifier was supplied.
	 */
	protected function resolveCalculator(string $identifier): CalculatorInterface
	{
		if (! $this->usesCalculators()) {
			throw new InvalidArgumentException('Calculator year was not set beforehand. Use `static::useCalculators` method.');
		}

		return CalculatorResolver::resolveCalculator($identifier, $this->calculatorYear);
	}
}
