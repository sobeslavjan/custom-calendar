<?php declare(strict_types = 1);

namespace App\Services\Calendar\DataSources\Traits;

use App\Model\DTOs\Calendar\Entry;
use App\Services\Calendar\WildcardProcessors\WildcardProcessorInterface;
use App\Services\Calendar\WildcardProcessors\WildcardProcessorResolver;
use InvalidArgumentException;

trait UsesWildcardProcessors
{

	/** @var integer|null $wildcardProcessorYear */
	protected ?int $wildcardProcessorYear;

	/**
	 * @param integer|null $wildcardProcessorYear
	 *
	 * @return void
	 */
	public function useWildcardProcessors(?int $wildcardProcessorYear = null): void
	{
		$this->wildcardProcessorYear = $wildcardProcessorYear;
	}

	/**
	 * @return boolean
	 */
	public function usesWildcardProcessors(): bool
	{
		return ! empty($this->wildcardProcessorYear);
	}

	/**
	 * @param Entry|null $entry If supplied, Entry aware processors will be returned as well.
	 *
	 * @return WildcardProcessorInterface[]
	 */
	protected function getWildcardProcessors(?Entry $entry = null): array
	{
		if (!$this->usesWildcardProcessors()) {
			throw new InvalidArgumentException('Wildcard processor year was not set beforehand. Use `static::useWildcardProcessors` method.');
		}

		$processors = WildcardProcessorResolver::universal($this->wildcardProcessorYear);

		if ($entry !== null) {
			$processors = array_merge(
				$processors,
				WildcardProcessorResolver::entryAware($this->wildcardProcessorYear, $entry)
			);
		}

		return $processors;
	}
}
