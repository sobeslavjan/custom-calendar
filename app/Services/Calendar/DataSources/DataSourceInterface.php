<?php declare(strict_types=1);

namespace App\Services\Calendar\DataSources;

use Generator;

interface DataSourceInterface
{

	/**
	 * @param integer|null $calculatorYear Year that will be used for calculations of variable dates (e.g. moving holidays).
	 * @return void
	 *
	 * @see Traits\UsesCalculators
	 */
	public function useCalculators(?int $calculatorYear = null): void;

	/**
	 * @return boolean
	 *
	 * @see Traits\UsesCalculators
	 */
	public function usesCalculators(): bool;

	/**
	 * @param integer|null $wildcardYear
	 * @return void
	 *
	 * @see Traits\UsesWildcardProcessors
	 */
	public function useWildcardProcessors(?int $wildcardYear = null): void;

	/**
	 * @return boolean
	 *
	 * @see Traits\UsesWildcardProcessors
	 */
	public function usesWildcardProcessors(): bool;

	/**
	 * Iterate over input and yield one date at the time.
	 *
	 * @return Generator
	 */
	public function iterate(): Generator;
}
