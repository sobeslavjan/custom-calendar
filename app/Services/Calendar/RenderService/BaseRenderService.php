<?php declare(strict_types = 1);

namespace App\Services\Calendar\RenderService;

use App\Model\DTOs\Calendar\CalendarData;
use App\Model\DTOs\Calendar\Entry;
use App\Model\Entities\Calendar\Calendar;
use App\Services\Calendar\Asset;
use App\Services\Calendar\DataSources\DataSourceInterface;
use App\Services\Calendar\DataSources\Plain\LocalStorageDataSource;
use App\Services\Calendar\DataSources\Plain\PresetDataSource;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;

class BaseRenderService implements RenderServiceInterface
{

	/**
	 * Main method.
	 *
	 * @param Calendar $calendar
	 *
	 * @return View
	 */
	public function render(Calendar $calendar): View
	{
		return $this->createView(
			$this->prepareData($calendar)
		);
	}

	/**
	 * Create view based on CalendarData.
	 *
	 * @param CalendarData $calendarData
	 *
	 * @return View
	 */
	public function createView(CalendarData $calendarData): View
	{
		App::setLocale($calendarData->locale);
		return view(
			'app.calendar._partial.render',
			['calendarData' => $calendarData]
		);
	}

	/**
	 * Construct necessary CalendarData.
	 *
	 * @param Calendar $calendar
	 *
	 * @return CalendarData
	 */
	public function prepareData(Calendar $calendar): CalendarData
	{
		// Init DTO.
		$calendarData = CalendarData::build(
			$calendar->year,
			$calendar->locale,
			$calendar->visual_preferences ?? []
		);

		// Append assets.
		$entriesAsset   = Asset::local(Asset::ASSET_TYPE_ENTRIES, $calendar->uuid);
		$frontPageAsset = Asset::local(Asset::ASSET_TYPE_FRONT_PAGE, $calendar->uuid);
		$lastPageAsset = Asset::local(Asset::ASSET_TYPE_LAST_PAGE, $calendar->uuid);
		$styleAsset     = Asset::local(Asset::ASSET_TYPE_STYLE, $calendar->uuid);

		foreach (array_filter([$entriesAsset, $frontPageAsset, $lastPageAsset, $styleAsset]) as $asset) {
			$calendarData->addAsset($asset);
		}

		// Prepare data sources.
		/** @var DataSourceInterface[] $dataSources */
		$dataSources = [];

		// Load preset data.
		foreach ($calendar->data_presets as $presetName) {
			$dataSources[] = new PresetDataSource($presetName);
		}

		// Load user data.
		if ($entriesAsset !== null) {
			$dataSources[] = new LocalStorageDataSource($entriesAsset->getFilename());
		}

		// Use data sources to fill CalendarData.
		foreach ($dataSources as $dataSource) {
			$dataSource->useCalculators($calendarData->year);
			$dataSource->useWildcardProcessors($calendarData->year);

			foreach ($dataSource->iterate() as $entry) {
				/** @var Entry $entry */
				$calendarData->addEntry($entry);
			}
		}

		return $calendarData;
	}

	/**
	 * Forget pre-calculated data.
	 *
	 * @param Calendar $calendar
	 *
	 * @return void
	 */
	public function flushData(Calendar $calendar): void
	{
		// There are no pre-calculated data.
	}
}
