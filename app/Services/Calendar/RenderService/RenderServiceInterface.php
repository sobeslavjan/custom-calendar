<?php declare(strict_types = 1);

namespace App\Services\Calendar\RenderService;

use App\Model\DTOs\Calendar\CalendarData;
use App\Model\Entities\Calendar\Calendar;
use Illuminate\View\View;

interface RenderServiceInterface
{

	/**
	 * Main method.
	 *
	 * @param Calendar $calendar
	 *
	 * @return View
	 */
	public function render(Calendar $calendar): View;

	/**
	 * Create view based on CalendarData.
	 *
	 * @param CalendarData $calendarData
	 *
	 * @return View
	 */
	public function createView(CalendarData $calendarData): View;

	/**
	 * Construct necessary CalendarData.
	 *
	 * @param Calendar $calendar
	 *
	 * @return CalendarData
	 */
	public function prepareData(Calendar $calendar): CalendarData;

	/**
	 * Forget pre-calculated data, if there are any.
	 *
	 * @param Calendar $calendar
	 *
	 * @return void
	 */
	public function flushData(Calendar $calendar): void;
}
