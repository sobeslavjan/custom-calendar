<?php declare(strict_types = 1);

namespace App\Services\Calendar\RenderService;

use App\Model\DTOs\Calendar\CalendarData;
use App\Model\Entities\Calendar\Calendar;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class CachedRenderService implements RenderServiceInterface
{

	/** @var int RENDER_CACHE_TTL Render cache in seconds. */
	protected const RENDER_CACHE_TTL = 600;

	/** @var string RENDER_CACHE_KEY Sprintf-ready render cache key. Append calendar UUID. */
	protected const RENDER_CACHE_KEY = 'calendar-render-%s';

	/** @var RenderServiceInterface */
	private $renderService;

	/**
	 * CachedRenderServiceRenderService constructor.
	 *
	 * @param RenderServiceInterface $renderService
	 */
	public function __construct(RenderServiceInterface $renderService)
	{
		$this->renderService = $renderService;
	}

	/**
	 * Main method.
	 *
	 * @param Calendar $calendar
	 *
	 * @return View
	 */
	public function render(Calendar $calendar): View
	{
		return $this->createView(
			$this->prepareData($calendar)
		);
	}

	/**
	 * Create view based on CalendarData.
	 *
	 * @param CalendarData $calendarData
	 *
	 * @return View
	 */
	public function createView(CalendarData $calendarData): View
	{
		return $this->renderService->createView($calendarData);
	}

	/**
	 * Construct necessary CalendarData.
	 *
	 * @param Calendar $calendar
	 *
	 * @return CalendarData
	 */
	public function prepareData(Calendar $calendar): CalendarData
	{
		return Cache::remember(
			sprintf(static::RENDER_CACHE_KEY, $calendar->uuid),
			static::RENDER_CACHE_TTL,
			function () use ($calendar) {
				return $this->renderService->prepareData($calendar);
			}
		);
	}

	/**
	 * Forget pre-calculated data.
	 *
	 * @param Calendar $calendar
	 *
	 * @return void
	 */
	public function flushData(Calendar $calendar): void
	{
		Cache::forget(sprintf(static::RENDER_CACHE_KEY, $calendar->uuid));
	}
}
