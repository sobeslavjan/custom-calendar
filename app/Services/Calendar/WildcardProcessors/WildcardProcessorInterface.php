<?php

namespace App\Services\Calendar\WildcardProcessors;

interface WildcardProcessorInterface
{

	/**
	 * @param string $input Unprocessed string, e.g. `last sunday of {year}`.
	 *
	 * @return string Processed string, e.g. `last sunday of 2021`.
	 */
	public function parse(string $input): string;
}
