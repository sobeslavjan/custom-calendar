<?php

namespace App\Services\Calendar\WildcardProcessors;

use App\Model\DTOs\Calendar\Entry;

abstract class EntryAwareWildcardProcessor implements WildcardProcessorInterface
{

	/**
	 * @param integer $year
	 * @param Entry   $entry
	 */
	public function __construct(
		protected int   $year,
		protected Entry $entry
	) {
	}
}
