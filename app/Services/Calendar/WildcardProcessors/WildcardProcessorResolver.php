<?php declare(strict_types=1);

namespace App\Services\Calendar\WildcardProcessors;

use App\Model\DTOs\Calendar\Entry;

class WildcardProcessorResolver
{

	/**
	 * @param integer $processorYear
	 *
	 * @return UniversalWildcardProcessor[]
	 */
	public static function universal(int $processorYear): array
	{
		return [
			// Date.
			new Date\YearWildcardProcessor($processorYear),
		];
	}

	/**
	 * @param integer $processorYear
	 * @param Entry   $entry
	 *
	 * @return EntryAwareWildcardProcessor[]
	 */
	public static function entryAware(int $processorYear, Entry $entry): array
	{

		return [
			// Date.
			new Date\AgeWildcardProcessor($processorYear, $entry),
			new Date\EntryYearWildcardProcessor($processorYear, $entry),
		];
	}
}
