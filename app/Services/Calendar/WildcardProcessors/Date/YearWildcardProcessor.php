<?php

namespace App\Services\Calendar\WildcardProcessors\Date;

use App\Services\Calendar\WildcardProcessors\WildcardIdentifiers;
use App\Services\Calendar\WildcardProcessors\UniversalWildcardProcessor;

class YearWildcardProcessor extends UniversalWildcardProcessor
{

	/**
	 * @param string $input Unprocessed string, e.g. `last sunday of {year}`.
	 *
	 * @return string Processed string, e.g. `last sunday of 2021`.
	 */
	public function parse(string $input): string
	{
		return str_replace(
			sprintf('{%s}', WildcardIdentifiers::YEAR),
			(string) $this->year,
			$input
		);
	}
}
