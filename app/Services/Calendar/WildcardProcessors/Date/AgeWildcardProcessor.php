<?php

namespace App\Services\Calendar\WildcardProcessors\Date;

use App\Services\Calendar\WildcardProcessors\EntryAwareWildcardProcessor;
use App\Services\Calendar\WildcardProcessors\WildcardIdentifiers;
use Carbon\Carbon;

class AgeWildcardProcessor extends EntryAwareWildcardProcessor
{

	/**
	 * @param string $input Unprocessed string, e.g. `together for {age} years`.
	 *
	 * @return string Processed string, e.g. `together for 3 years`.
	 */
	public function parse(string $input): string
	{
		$entryDate = $this->entry->date;
		$currentDate = $entryDate->clone()->year($this->year);

		return str_replace(
			sprintf('{%s}', WildcardIdentifiers::AGE),
			(string) $currentDate->diffInYears($entryDate),
			$input
		);
	}
}
