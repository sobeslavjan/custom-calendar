<?php

namespace App\Services\Calendar\WildcardProcessors\Date;

use App\Services\Calendar\WildcardProcessors\EntryAwareWildcardProcessor;
use App\Services\Calendar\WildcardProcessors\WildcardIdentifiers;

class EntryYearWildcardProcessor extends EntryAwareWildcardProcessor
{

	/**
	 * @param string $input Unprocessed string, e.g. `{entry-year}`.
	 *
	 * @return string Processed string, e.g. `1934`.
	 */
	public function parse(string $input): string
	{
		return str_replace(
			sprintf('{%s}', WildcardIdentifiers::ENTRY_YEAR),
			(string) $this->entry->date->year,
			$input
		);
	}
}
