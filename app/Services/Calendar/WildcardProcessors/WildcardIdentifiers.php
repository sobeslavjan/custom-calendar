<?php declare(strict_types=1);

namespace App\Services\Calendar\WildcardProcessors;

use Consistence\Enum\Enum;

class WildcardIdentifiers extends Enum
{

	// Date.
	public const YEAR = 'year';
	public const AGE = 'age';
	public const ENTRY_YEAR = 'entry-year';
}
