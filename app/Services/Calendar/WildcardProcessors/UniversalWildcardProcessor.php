<?php

namespace App\Services\Calendar\WildcardProcessors;

abstract class UniversalWildcardProcessor implements WildcardProcessorInterface
{

	/**
	 * @param integer $year
	 */
	public function __construct(protected int $year)
	{
	}
}
