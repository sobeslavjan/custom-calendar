<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Throwable;

class Handler extends ExceptionHandler
{

	/**
	 * A list of the exception types that are not reported.
	 *
	 * @var array<int, class-string<\Throwable>>
	 */
	protected $dontReport = [];

	/**
	 * A list of the inputs that are never flashed for validation exceptions.
	 *
	 * @var array<int, string>
	 */
	protected $dontFlash = [
		'password',
		'password_confirmation',
	];

	/**
	 * Report or log an exception.
	 *
	 * @param Throwable $e
	 * @return void
	 * @throws Throwable
	 */
	public function report(Throwable $e)
	{
		parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param mixed     $request Request.
	 * @param Throwable $e
	 * @return JsonResponse|RedirectResponse|Response|\Symfony\Component\HttpFoundation\Response
	 * @throws Throwable
	 *
     * phpcs:disable Squiz.Commenting.FunctionComment.TypeHintMissing
	 */
	public function render($request, Throwable $e)
	{
		return parent::render($request, $e);
	}
}
