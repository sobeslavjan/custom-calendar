<?php declare(strict_types = 1);

namespace App\Helpers\UI;

use Illuminate\Support\Facades\Session;

class Notification
{

	/** @var string SESSION_KEY */
	protected const SESSION_KEY = 'ui-notification';

	/**
	 * @param string $message
	 *
	 * @return void
	 */
	public static function info(string $message): void
	{
		static::set($message, 'info');
	}

	/**
	 * @param string $message
	 *
	 * @return void
	 */
	public static function success(string $message): void
	{
		static::set($message, 'success');
	}

	/**
	 * @param string $message
	 *
	 * @return void
	 */
	public static function warning(string $message): void
	{
		static::set($message, 'warning');
	}

	/**
	 * @param string $message
	 *
	 * @return void
	 */
	public static function error(string $message): void
	{
		static::set($message, 'danger');
	}

	/**
	 * Set temporary notification to user.
	 *
	 * @param string $message
	 * @param string $type
	 *
	 * @return void
	 */
	public static function set(string $message, string $type = 'info'): void
	{
		Session::flash(
			static::SESSION_KEY,
			[
				'message'     => $message,
				'alert-class' => $type,
			]
		);
	}

	/**
	 * Get if any notification is set.
	 *
	 * @return boolean
	 */
	public static function isSet(): bool
	{
		return Session::has(static::SESSION_KEY);
	}

	/**
	 * Get contetns of current notification.
	 *
	 * @return array
	 */
	public static function get(): array
	{
		return Session::get(static::SESSION_KEY);
	}
}
