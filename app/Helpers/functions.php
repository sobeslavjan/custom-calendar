<?php declare(strict_types = 1);

if (! function_exists('translate')) {
	/**
	 * @param string $stringKey
	 *
	 * @return string
	 */
	function translate(string $stringKey): string
	{
		$translated = __($stringKey);
		return is_string($translated) ? $translated : '';
	}
}
