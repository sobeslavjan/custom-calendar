<?php declare(strict_types = 1);

namespace App\Helpers;

class Regex
{

	public const EMPTY        = '/^$/';
	public const NUMERIC      = '/^\d+$/';
	public const DECIMAL      = '/^[\d\.]+$/';
	public const ALPHANUMERIC = '/^[0-9a-zA-Z]+$/';
	public const URL          = '/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i';
	public const DATE         = '/^\d{4}\-\d{2}\-\d{2}$/';

	/**
	 * @param string $string
	 * @param string $regex
	 *
	 * @return boolean
	 */
	public static function match(string $string, string $regex): bool
	{
		return (bool) preg_match($regex, $string);
	}
}
