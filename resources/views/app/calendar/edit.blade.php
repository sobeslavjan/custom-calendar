@extends('app._layout')

@section('content')

    <calendar-edit-form
            uuid="{{ $calendar->uuid }}"
            defaults=" {{ json_encode($calendar) }}"
            constants=" {{ json_encode($constants) }}"
    >
        @include('app.calendar._partial.searchForm')
    </calendar-edit-form>

@endsection