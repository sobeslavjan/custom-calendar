<form method="get" action="/calendar/search" role="form" class="calendar-search-form">
    <div class="form-group">
        <label for="uuid">
            Find calendar
        </label>

        <div class="input-group">
            <input name="uuid"
                   id="uuid"
                   placeholder="24003083-6953-4169-8290-5c4f303c0284"
                   class="form-control"
                   value="{{ $calendar->uuid ?? old('uuid') }}"
            />
            <div class="input-group-append">
                <button class="btn btn-primary"> Search </button>
            </div>
        </div>

        @if($errors->has('uuid'))
            <small class="text-danger">{{ $errors->first('number') }}</small>
        @endif
    </div>
</form>