<?php
/* @var \App\Model\DTOs\Calendar\CalendarData $calendarData */
?>

<section class="render__scope {{$calendarData->size}} template--{{$calendarData->template}} colour-schema--{{$calendarData->colourSchema}}">
    <style>
        @page {
            size: {{$calendarData->size}}
        }

        @isset($calendarData->assets[Asset::ASSET_TYPE_STYLE])
            {{ $calendarData->assets[Asset::ASSET_TYPE_STYLE]->getContents() }}
        @endisset
    </style>

    @isset($calendarData->assets[Asset::ASSET_TYPE_FRONT_PAGE])
        <section class="sheet page page--front">
            {!! $calendarData->assets[Asset::ASSET_TYPE_FRONT_PAGE]->getContents() !!}
        </section>
    @endisset

    @foreach($calendarData->months as $monthIndex => $month)
        {{-- SHEET start --}}
        <section class="sheet page page--{{$monthIndex}}">

            {{-- MONTH start --}}
            <div class="calendar__wrapper">
                <table class="calendar calendar--index-{{$monthIndex}}">

                    {{-- table heading --}}
                    <caption class="calendar__banner">
                        <div class="calendar__banner__heading">
                            <h1 class="calendar__banner__heading__inner">
                                {{-- month number --}}
                                @if($calendarData->renderMonthNumber)
                                    [{{$monthIndex}}]
                                @endif

                                {{-- month name --}}
                                {{translate("calendar/render.months.$monthIndex")}}
                            </h1>
                        </div>

                        <div class="calendar__banner__images">

                            @foreach($month->getEntriesWithImages() as $entry)
                                <div class="calendar__banner__image">
                                    <img src="{{$entry->image}}"/>
                                </div>
                            @endforeach

                        </div>
                    </caption>

                    {{-- column headings --}}
                    <thead class="calendar__head">
                        <tr class="calendar__head__row">
                            {{-- week number --}}
                            @if($calendarData->renderWeekNumber)
                                <th class="
                                        calendar__head__cell
                                        calendar__head__cell--week-number
                                        "
                                >
                                    #
                                </th>
                            @endif

                            {{-- days --}}
                            @for($dayIndex = 1; $dayIndex <= 7; $dayIndex++)
                                <th class="calendar__head__cell">
                                    {{-- day number --}}
                                    @if($calendarData->renderDayNumber)
                                        [{{$dayIndex}}]
                                    @endif

                                    {{-- day name --}}
                                    {{translate("calendar/render.days.$dayIndex")}}
                                </th>
                            @endfor
                        </tr>
                    </thead>

                    {{-- contents --}}
                    <tbody class="calendar__body">

                        @foreach($month->weeks as $weekIndex => $week)
                            {{-- WEEK start--}}
                            <tr class="
                                    calendar__body__row
                                    calendar__week
                                    calendar__week--index-{{$weekIndex}}
                                    "
                            >

                                {{-- week number --}}
                                @if($calendarData->renderWeekNumber)
                                    <td class="
                                            calendar__body__cell
                                            calendar__body__cell--week-number
                                            "
                                    >
                                    {{$week->weekOfYear}}
                                    </th>
                                @endif

                                {{-- days --}}
                                @foreach($week->days as $dayIndex => $day)
                                    {{-- DAY start--}}
                                    <td class="
                                            calendar__body__cell
										    calendar__day
										    calendar__day--index-{{$dayIndex}}
                                            calendar__day--highlight-{{$day->getHighestHighlight()}}
                                    @foreach($day->getClasses() as $class)
                                            calendar__day--{{$class}}
                                    @endforeach
                                            "
                                    >
                                        {{-- day number --}}
                                        <div class="calendar__day__number">
                                            {{$day->dayOfMonth ?? ''}}
                                        </div>

                                        {{-- icons --}}
                                        <div class="calendar__day__icons">
                                            @foreach($day->getEntriesWithIcons() as $entry)

                                                <span class="calendar__icon">
                                                    {{$entry->icon}}
                                                </span>

                                            @endforeach
                                        </div>

                                        {{-- events --}}
                                        <div class="calendar__day__events">
                                            @foreach($day->getEntriesWithCaptions() as $entry)

                                                <span class="
                                                        calendar__event
                                                        calendar__event--highlight-{{$entry->highlight}}
                                                @if(!empty($entry->class))
                                                        calendar__event--{{$entry->class}}
                                                @endif
                                                        "
                                                >
                                                    <!-- {{ $entry->hash }} -->
                                                    {{$entry->caption}}
                                                </span>

                                            @endforeach
                                        </div>

                                    </td>
                                    {{-- DAY end --}}
                                @endforeach

                            </tr>
                            {{-- WEEK end --}}
                        @endforeach

                    </tbody>

                    <tfoot class="calendar__footer">
                        <tr>
                            <td>

                                @foreach($month->getEntriesWithNotes() as $entry)
                                    <span class="calendar__footer__entry">
                                        <strong>{{ $entry->caption }}</strong>: {{ $entry->note }}
                                    </span>
                                @endforeach

                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            {{-- MONTH end --}}

        </section>
        {{-- SHEET end --}}
    @endforeach

    @isset($calendarData->assets[Asset::ASSET_TYPE_LAST_PAGE])
        <section class="sheet page page--last">
            {!! $calendarData->assets[Asset::ASSET_TYPE_LAST_PAGE]->getContents() !!}
        </section>
    @endisset
</section>
