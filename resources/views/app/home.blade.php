@extends('app._layout')

@section('content')

    <section class="home container">

        <div class="window">

            <div class="mb-2">
                @include('app.calendar._partial.searchForm')

            </div>

            <div class="mb-2">
                or
            </div>

            <div>
                <a href="{{ URL::to('/calendar/create') }}"
                   class="btn btn-primary"
                >
                    Create new
                </a>
            </div>

        </div>

    </section>

@endsection