<!DOCTYPE html>
<html @isset($meta) lang="{{ $meta->lang }}" @endisset>
    <head>
        {{-- Indexing robots meta. --}}
        @isset($meta)
            <meta name="robots" content="{{ $meta->robots }}"/>
            @if($meta->canonicalUri !== null)
                <link rel="canonical" href="{{ $meta->canonicalUri }}"/>
            @endif
        @endisset

        {{-- Language meta. --}}
        @isset($meta)
            <meta http-equiv="content-type" content="text/html;charset={{ $meta->charset }}">
            <meta charset="{{ $meta->charset }}"/>
        @endisset

        {{-- Page contents meta. --}}
        @isset($meta)
            <title>{{ $meta->title }}</title>
            <meta name="description" content="{{ $meta->description }}"/>
            @if(strlen($meta->keywords) > 5)
                <meta name="keywords" content="{{ $meta->keywords }}"/>
            @endif
        @endisset

        {{-- Author meta. --}}
        @isset($meta)
            <meta name="author" content="{{ $meta->author }}"/>
            <link rel="author" href="{{ $meta->authorLink }}"/>
        @endisset

        {{-- opengraph meta --}}
        @isset($meta)
            <meta property="og:type" content="website"/>
            <meta property="og:url" content="{{ URL::current() }}"/>
            <meta property="og:locale" content="{{ $meta->locale }}"/>
            <meta property="og:site_name" content="{{ $meta->rootTitle }}"/>
            <meta property="og:title" content="{{ $meta->title }}"/>
            <meta property="og:description" content="{{ $meta->description }}"/>
            <meta property="og:image" content="{{ $meta->image }}"/>
            <meta property="og:image:type" content="{{ $meta->imageMime }}"/>
        @endisset

        {{-- twitter data--}}
        @isset($meta)
            <meta name="twitter:card" content="summary">
            <meta name="twitter:url" content="{{ URL::current() }}">
            <meta name="twitter:title" content="{{ $meta->title }}">
            <meta name="twitter:description" content="{{ $meta->description }}">
            <meta name="twitter:image" content="{{ $meta->image }}">
        @endisset

        {{-- favicon --}}
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        {{-- styles --}}
        <link rel="stylesheet" href="{{ URL::to('/') }}/css/app.css">

        {{-- app related meta data --}}
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body id="app">

        <header class="top-bar">

            <div class="navigation-header">
                <div class="branding container">

                    <a href="{{ URL::to('/') }}">
                        <img src="{{ URL::to('/logo.png') }}"
                             alt=" @isset($meta) {{ $meta->title }} @endisset"
                             class="branding--logo"
                        >
                    </a>

                    <h1 class="branding--heading">
                        @isset($meta)
                            {{ $meta->rootTitle }}
                        @else
                            App
                        @endisset
                    </h1>

                </div>
            </div>

            @include('_components.UI.notification')
        </header>

        <main class="main-content">
            @yield('content')
        </main>

        {{-- scripts --}}
        <script type="text/javascript">
            window.Laravel = {
                "csrfToken": "{{csrf_token() }}",
                "baseUrl": "{{ URL::to('/') }}",
            }
        </script>
        <script type="text/javascript" src="{{ URL::to('/') }}/js/app.js"></script>
    </body>
</html>