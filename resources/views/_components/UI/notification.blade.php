@if(\App\Helpers\UI\Notification::isSet())
    <p class="alert text-center alert-{{ \App\Helpers\UI\Notification::get()['alert-class'] }}" role="alert">
        {{ \App\Helpers\UI\Notification::get()['message'] }}
    </p>
@endif