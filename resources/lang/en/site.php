<?php

return [
	// Language.
	'lang'        => 'en',
	'locale'      => 'EN-GB',
	'charset'     => 'UTF-8',

	// Page contents.
	'rootTitle'   => 'Custom Calendar',
	'description' => 'Printable calendar with custom dates and images',
	'keywords'    => 'custom, customizable, own, printable, yearly, monthly, calendar',
	'image'       => 'favicon-original.png',
	'imageMime'   => 'image/png',

	// Author.
	'author'      => 'Jan Sobeslav',
	'authorLink'  => 'https://sobeslav.net',
];