<?php

return [
	'months' => [
		1  => 'Leden',
		2  => 'Únor',
		3  => 'Březen',
		4  => 'Duben',
		5  => 'Květen',
		6  => 'Červen',
		7  => 'Červenec',
		8  => 'Srpen',
		9  => 'Září',
		10 => 'Říjen',
		11 => 'Listopad',
		12 => 'Prosinec',
	],
	'days'   => [
		1 => 'pondělí',
		2 => 'úterý',
		3 => 'středa',
		4 => 'čtvrtek',
		5 => 'pátek',
		6 => 'sobota',
		7 => 'neděle',
	],
];