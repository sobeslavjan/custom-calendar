# Test suite

Following QA tools should be registered in CI and run on every feature branch before merge to develop is approved.

There are shortcuts for these commands in the root Makefile.

Following sections documents any notable caveats.

## parallel-lint

Checks if there are any syntax errors.

Run: `php vendor/bin/parallel-lint app/ database/ routes/`.

## phpcs

Code style checks.

Run: `php vendor/bin/phpcs --standard=phpcs_ruleset.xml app --ignore=*\.js,*\.css,*\.sh,*\.md`.

If there are violations that are justifiable to be accepted, it's possible to disable checking of section of code by wrapping it between comments e.g. `phpcs:disable Generic.Files.LineLength.TooLong` and `phpcs:enable`.

In order to find out what rule was violated, run the command with `-s` parameter.

See config: `phpcs_ruleset.xml`.

See docs: https://github.com/squizlabs/PHP_CodeSniffer

Note: Also check out related `phpcbf` command: it attempts to fix the violations automatically.

## phpmd

Code smell detector.

Run: `php vendor/bin/phpmd app text phpmd_ruleset.xml`

If there are violations that are justifiable to be accepted (e.g. `CouplingBetweenObjects` in service provider), it's possible to disable checking of section of code by phpdoc annotation `@SuppressWarnings(ExcessiveMethodLength)`.

In order to find out what rule was violated, run the command with output parameter set to `json`, instead of `text`, e.g. `php vendor/bin/phpmd app json phpmd_ruleset`.

There may be certain violations that couldn't be fixed at the moment when phpmd was introduced. These are listed in baseline file and therefore ignored. If it's utterly necessary to extend the baseline, it's possible with following command.

Generate baseline: `php vendor/bin/phpmd app text phpmd_ruleset.xml --generate-baseline`

If there's file that causes internal failure during processing (e.g., new language constructs), it can be excluded by configuration: `<exclude-pattern>*ExampleFile.php</exclude-pattern>`.

See config: `phpmd_ruleset.xml`.

See docs: https://phpmd.org/documentation/index.html 

## phpstan

Static analysis tool.

Run: `php vendor/bin/phpstan analyse --memory-limit=2G`

Justifiable violations can be ignored next to the violating line of code using PHPDoc tags in comments:

```
@phpstan-ignore-line
@phpstan-ignore-next-line
```

There may be certain violations that couldn't be fixed at the moment when phpstan was introduced. These are listed in baseline file and therefore ignored. If it's utterly necessary to extend the baseline, it's possible with following command.

Generate baseline: `php vendor/bin/phpstan analyse --memory-limit=2G --generate-baseline`

See config: `phpstan.neon`.

See docs: https://phpstan.org/user-guide/getting-started 

## phpunit

@todo
