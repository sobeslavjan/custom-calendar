# Custom data

- Create a new Calendar entity, make note of its UUID
- Copy `/docs/custom-data-example/` to `/storage/app/user-data/{uuid}/`
- Copy your custom images to `/storage/app/public/{uuid}/`
- In the custom data, refer to the images with prefix `/storage/{uuid}/`
- Make backup of your custom entries, you might need it next year

## entries.json

- `date` - one of following values:
  - date in short ISO format (e.g. `1970-01-01`)
  - Carbon compliant string (e.g. `last sunday of march 2021`)
  - calculator ID (see `\App\Services\Calendar\Calculators\CalculatorIdentifiers`)
- `caption` - entry name rendered in the calendar
- `note` - details rendered bellow the calendar
- `icon` - content rendered in the calendar
- `highlight` - level of highlight: 1-5
- `image` - public path to the image
- `hash` - you can fill it explicitly in order to override an event from a preset; see comment in `.calendar__event` element 

- `date` and string fields (`caption`, `note`, `icon`) may contain wildcards (see `\App\Services\Calendar\WildcardProcessors\WildcardIdentifiers`)

- invalid JSON entries will be ignored; you can use strings for comments
