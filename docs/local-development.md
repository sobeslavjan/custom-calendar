# Local development

The project is developed in Docker.

If this is your first time running the project locally:

- create `.env` file based on `.env.example`

Then, and on every subsequent run:

- run `docker-compose up -d --build`

If you want to do fresh reinstall:

- stop all services
- you might want to remove mysql data in `/docker/cache/mysql`
- you might want to remove packages in `/vendor`, `/node_modules`
- run `docker-compose up -d --build --force-recreate`

From the app container:

- run `composer install`
- run `npm install`
- run `php artisan migrate`
- run `php artisan storage:link`
- (or, `make install`)

Then, all should be up and running:

- App: http://localhost
- HTTPs should be available, but your browser may have an issue with self-signed certificate.
- DB adminer: http://localhost:8001/
    - Server: `mysql`
    - User: `app` / `app`
    - Root: `root` / `root`
    - DB: `custom-calendar`
- If there are any issues with these credentials, check the docker-compose file, Dockerfile and install script. If readme is not up-to-date, update it.

## Debugging

The docker files include Xdebug integration. In order to debug both HTTP and CLI connections in PhpStorm, setup two servers:

| Name                 | Host                 | Port |
|----------------------|----------------------|------|
| host.docker.internal | host.docker.internal | 80   |
| localhost            | localhost            | 80   |

On both of them, enable path mapping, and set root directory to point to `/app`

## Local environment troubleshooting

**Docker is slow on Windows**

- follow official Docker docs: https://docs.docker.com/desktop/windows/wsl/
- make sure you use WSL2, and install Ubuntu 20.04 LTS subsystem from Microsoft store; configure Docker Windows app to use Ubuntu
- store your project files in the subsystem directly: `\\wsl$\Ubuntu-20.04` - inside the subsystem, this path equals to root - `\`
- run Docker commands (`e.g. docker compose up`) directly from Ubuntu subsystem
- in Windows apps (e.g. PHPStorm), open the project using the WSL path, e.g. `\\wsl.localhost\Ubuntu-20.04\home\ubuntu\docker_projects\project`
- you can also create symlink: `mklink /d C:\Users\xxxx\docker_projects \\wsl.localhost\Ubuntu-20.04\home\ubuntu\docker_projects` (you need to run the CMD as admin)

**Permission issues**

- If you have problem running mysql server, or shell scripts, try running `chmod -R 0777 ./docker` in your host system (Ubuntu WSL, in case of Windows).
