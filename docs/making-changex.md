# Making changes

Before submitting merge request, from the app container:

- run `make qa-fix`
- run `make qa`
- run `make build`
- (or `make commit`)

## Presets

- Use the example `entries.json` from custom data example, use it the same way as described in `custom-data.md`
