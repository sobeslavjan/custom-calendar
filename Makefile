env ?= local

install:
	composer install
	npm install
	$(MAKE) db-migrate
	php artisan storage:link

commit:
	$(MAKE) qa-fix
	$(MAKE) qa
	$(MAKE) build

build:
	npm run prod

qa:
	$(MAKE) qa-parallel-lint
	$(MAKE) qa-phpcs
	$(MAKE) qa-phpmd
	$(MAKE) qa-phpstan
	# $(MAKE) qa-phpunit

qa-fix:
	$(MAKE) qa-phpcbf

qa-parallel-lint:
	vendor/bin/parallel-lint app/ database/ routes/

qa-phpcs:
	vendor/bin/phpcs --standard=phpcs_ruleset.xml app --ignore=*\.js,*\.css,*\.sh,*\.md

qa-phpcbf:
	vendor/bin/phpcbf --standard=phpcs_ruleset.xml app --ignore=*\.js,*\.css,*\.sh,*\.md

qa-phpmd:
	vendor/bin/phpmd app text phpmd_ruleset.xml

qa-phpstan:
	vendor/bin/phpstan analyse --memory-limit=2G

qa-codecept: # todo: replace with phpunit
	vendor/bin/codecept run

qa-phpunit:
	vendor/bin/phpunit --exclude="legacy,example"

qa-phpunit-group:
	vendor/bin/phpunit --group=$(name)

qa-phpunit-this:
	vendor/bin/phpunit --group="this"

qa-phpunit-coverage:
	XDEBUG_MODE=coverage vendor/bin/phpunit --exclude="legacy,example" --coverage-text

db-migrate:
	php artisan migrate --env $(env)

cache-clear:
	php artisan cache:clear -e $(env)
	php artisan view:clear -e $(env)
	supervisorctl restart apache:apached
