<?php namespace Calendar;

use App\Http\Controllers\App\Calendar\CalendarController;
use App\Model\Entities\Calendar\Calendar;
use App\Services\Calendar\RenderService\RenderServiceInterface;
use FunctionalTester;
use Illuminate\Http\Request;

class CalendarControllerCest
{

    /**
     * @see CalendarController::update()
     */
    public function calendarUpdateWorks(FunctionalTester $I)
    {
        $I->wantToTest('that updating the calendar works as expected');

        // GIVEN THAT ...

        // There's a calendar.
        $calendarId = $I->haveInDatabase('calendars', [
            'uuid' => '982a3365-cfb1-4739-9b74-e740b3712fea',
            'year' => '2019',
            'locale' => 'en',
            'data_presets' => '[]',
            'visual_preferences' => '{"size": "A4"}',
        ]);

        // WHEN ...

        // Request to update that calendar is sent.
        $request = Request::create(
            '/calendars/982a3365-cfb1-4739-9b74-e740b3712fea',
            'POST',
            [
                'year' => 2020,
                'locale' => 'cs',
                'data_presets' => ['holiday-cs'],
                'visual_preferences' => [
                    'size' => 'A5',
                    'renderWeekNumber' => true,
                    'template' => 'default',
                    'colourSchema' => 'default',
                ],
            ]
        );

        $controller = app()->make(CalendarController::class);
        $controller->update(
            $request,
            Calendar::find($calendarId),
            app()->make(RenderServiceInterface::class)
        );

        // THEN ...
        $calendar = Calendar::find($calendarId);

        // The calendar has new contents in the DB.
        $I->assertEquals(2020, $calendar->year);
        $I->assertEquals('cs', $calendar->locale);
        $I->assertEquals(['holiday-cs'], $calendar->data_presets);
        $I->assertEquals(
            [
                'size' => 'A5',
                'renderWeekNumber' => true,
                'template' => 'default',
                'colourSchema' => 'default',
            ],
            $calendar->visual_preferences
        );
    }
}
